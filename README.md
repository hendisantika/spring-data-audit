# Spring Data Audit

Spring Data Audit Samples using `JPA` and `MongoDB`. In this repository and it's branches, I will demonstrate with examples how to setup a Spring Boot project to keep an audit log of database events.

Run this project by this command :

`mvn clean spring-boot:run`